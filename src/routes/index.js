require('module-alias/register')
const routes = require("express").Router()

const inventore = require("@routes/inventore")
const orders = require("@routes/orders")
const products = require("@routes/products")
const users = require("@routes/users")

routes.use("/inventore",inventore)
routes.use("/orders",orders)
routes.use("/products",products)
routes.use("/users",users)

module.exports = routes
