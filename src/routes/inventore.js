require("module-alias/register");
const routes = require("express").Router();
const inventore = require("@controllers/inventore/_index");
const fmiddlewares = require("fmiddlewares");
const apiKey = require("@middlewares/apiKey");

routes.get("/", [apiKey], inventore.get);
routes.post(
    "/",
    [
        apiKey,
        fmiddlewares.validateItem({
            amount: {
                type: "number",
            },
            product_id: {
                type: "number",
            },
        }),
    ],
    inventore.post
);

module.exports = routes;
