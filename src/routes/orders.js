require("module-alias/register");
const routes = require("express").Router();
const orders = require("@controllers/orders/_index");
const fmiddlewares = require("fmiddlewares");
const apiKey = require("@middlewares/apiKey");

routes.get("/", [apiKey], orders.get);
routes.post(
    "/",
    [
        apiKey,
        fmiddlewares.validateItem({
            user_id: {
                type: "string",
            },
            products: {
                type: "array",
            },
        }),
    ],
    orders.post
);
routes.put(
    "/",
    [
        apiKey,
        fmiddlewares.validateItem({
            data: {
                type: "object",
                items: {
                    status: {
                        type: "list",
                        list: ["pendiente", "procesando", "completado"],
                    },
                    products: {
                        type: "array",
                    },
                },
            },
            where: {
                type: "object",
                items: {
                    id: {
                        type: "string",
                    },
                },
            },
        }),
    ],
    orders.put
);

module.exports = routes;
