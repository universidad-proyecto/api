require("module-alias/register");
const routes = require("express").Router();
const products = require("@controllers/products/_index");
const fmiddlewares = require("fmiddlewares");
const apiKey = require("@middlewares/apiKey");
const validateToken = require("@middlewares/validateToken");


routes.get("/", [validateToken], products.get);
routes.delete(
    "/",
    [
        apiKey,
        fmiddlewares.validateItem(
            {
                id: {
                    type: "string",
                },
            },
            "query"
        ),
    ],
    products.delete
);
routes.post(
    "/",
    [
        apiKey,
        fmiddlewares.validateItem({
            name: {
                type: "string",
            },
            description: {
                type: "string",
            },
            img: {
                type: "string",
            },
            discount: {
                min:0,
                max:100,
                type: "number",
            },
            weigth: {
                min:0,
                type: "number",
            },
            width: {
                min:0,
                type: "number",
            },
            height: {
                min:0,
                type: "number",
            },
            length: {
                min:0,
                type: "number",
            },
            price: {
                type: "number"

            }
        }),
    ],
    products.post
);
routes.put(
    "/",
    [
        apiKey,
        fmiddlewares.validateItem({
            data: {
                type: "object",
                items: {
                    exactItems: true,
                    name: {
                        type: "string",
                    },
                    description: {
                        type: "string",
                    },
                    img: {
                        type: "string",
                    },
                    discount: {
                        min:0,
                        max:100,
                        type: "number",
                    },
                    weigth: {
                        min:0,
                        type: "number",
                    },
                    width: {
                        min:0,
                        type: "number",
                    },
                    height: {
                        min:0,
                        type: "number",
                    },
                    length: {
                        min:0,
                        type: "number",
                    },
                    price: {
                        type: "number"
        
                    }
                },
            },
            where: {
                type: "object",
                items: {
                    id: {
                        type: "string",
                    },
                },
            },
        }),
    ],
    products.put
);

module.exports = routes;
