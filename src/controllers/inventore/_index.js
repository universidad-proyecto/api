require('module-alias/register')

module.exports = {
    get: require("@controllers/inventore/get"),
    delete: require("@controllers/inventore/delete"),
    post: require("@controllers/inventore/post"),
    put: require("@controllers/inventore/put")
}