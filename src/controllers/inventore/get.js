require("module-alias/register");
const db = require("@app/pg");

const getInventore = async (req, res) => {
    try {
        const sql = `
        select * from inventore;
        `;

        const r = await db(sql);

        res.send({
            type: "ok",
            result: r.rows,
        });
    } catch (error) {
        return res.status(500).send({
            type: "error",
            error,
            msj: `${error}`,
        });
    }
};

module.exports = getInventore;
