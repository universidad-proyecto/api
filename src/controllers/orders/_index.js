require('module-alias/register')

module.exports = {
    get: require("@controllers/orders/get"),
    delete: require("@controllers/orders/delete"),
    post: require("@controllers/orders/post"),
    put: require("@controllers/orders/put")
}