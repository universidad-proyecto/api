require("module-alias/register");
const db = require("@app/pg");

const putProducts = async (req, res) => {
    try {
        const { where, data } = req.body;

        data.update_time = new Date().getTime();

        const colunms = Object.keys(data);
        const values = Object.values(data);
        const sql = `
        update products set (${colunms.join(",")}) = ('${values.join("','")}')
        where id ='${where.id}';
        `;

        const r = await db(sql);

        res.send({
            type: "ok",
            result: r,
        });
    } catch (error) {
        return res.status(500).send({
            type: "error",
            error,
            msj: `${error}`,
        });
    }
};

module.exports = putProducts
