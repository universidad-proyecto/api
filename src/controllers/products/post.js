require('module-alias/register')
const db = require("@app/pg")

const postProducts = async (req,res)=>{

    try {
        const data = req.body

        data.create_time=(new Date()).getTime()
        data.update_time=(new Date()).getTime()

        const colunms = Object.keys(data)
        const values = Object.values(data)
        const sql =`
        insert into products (${colunms.join(",")}) values('${values.join("','")}');
        `

        const r = await db(sql)

        res.send({
            type : "ok",
            result : r
        })
        
    } catch (error) {
        return res.status(500).send({
            type : "error",
            error ,
            msj :`${error}`  
        })
    }
    

}

module.exports = postProducts