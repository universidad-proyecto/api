require('module-alias/register')

module.exports = {
    get: require("@controllers/products/get"),
    delete: require("@controllers/products/delete"),
    post: require("@controllers/products/post"),
    put: require("@controllers/products/put")
}