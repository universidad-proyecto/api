require("module-alias/register");
const db = require("@app/pg");

const getProducts = async (req, res) => {
    try {
        const sql = `
        select * from products;
        `;

        const r = await db(sql);

        res.send({
            type: "ok",
            result: r.rows,
        });
    } catch (error) {
        return res.status(500).send({
            type: "error",
            error,
            msj: `${error}`,
        });
    }
};

module.exports = getProducts;
