console.log("api cargada")
const request = async (settings) => {
    try {
        const headersList = {
            "Accept": "*/*",
            "User-Agent": "Thunder Client (https://www.thunderclient.io)",
            "Content-Type": "application/json",
            "Authorization":`Bearer ${localStorage.getItem('token')}`
        }
        const config = { 
            method: settings.method,
            headers: headersList
        }
        if(settings.method != "GET"){
            config.body = JSON.stringify(settings.body)
        }
        const respond = await fetch(`http://localhost:3001/api/v1/${settings.rute}`, config)
        const result = await respond.json()
        
        if(result.token === "error"){
            console.log(result);
            localStorage.removeItem('token')
            window.location.href = "./login.html"
            throw new Error("exit")
        }

        return result
    } catch (error) {
        console.log("error",error);
        alert(`Server Error`)
        throw new Error("Error Connect Server")
    }
}


const addProducts = async (data) => {
    console.log(data)
    const result = await request({
        rute:"products",
        method:"POST",
        body:data
    })
    console.log(result);
    

    if(result.type == "ok"){
        return result.result
    }else{
        alert(result.msj)
    }
}
const deleteProducts = async (id) => {
    const result = await request({
        rute:`products?id=${id}`,
        method:"DELETE",
        body:{}
    })
    console.log(result);
    if(result.type == "ok"){
        return result.result
    }else{
        alert(result.msj)
    }
}
const updateProducts = async (data) => {
    const id = data.id
    delete data.id
    const result = await request({
        rute:`products`,
        method:"PUT",
        body: {
            data,
            where:{
                id
            }
        }
    })
    console.log(result);
    

    if(result.type == "ok"){
        return result.result
    }else{
        alert(result.msj)
    }
}
const getProducts = async () => {
    const result = await request({
        rute:"products",
        method:"GET",
    })
    console.log(result);

    if(result.type == "ok"){
        return result
    }else {
        alert(result.msj)
    }
}

const login = async (data) => {
    const result = await request({
        rute:"users/login",
        method:"POST",
        body:data
    })
    console.log(result);
    if(result.type == "ok"){
        return result
    }else{
        alert(result.msj)
        return result
    }
}
const register = async (data) => {
    const result = await request({
        rute:"users/register",
        method:"POST",
        body:data
    })
    console.log(result);
    if(result.type == "ok"){
        return result
    }else{
        alert(result.msj)
        return result
    }
}

