
const inputEmail =  document.getElementById("email")
const inputPassword =  document.getElementById("password")
const btnSubmint =  document.getElementById("btnSubmint")

btnSubmint.addEventListener('click',async (e)=>{
    e.preventDefault()
    const data = {
        email : inputEmail.value,
        password : inputPassword.value,
    }
    const result = await login(data);
    if(result.type == "ok"){
        const {token} = result 
        localStorage.setItem('token',token)
        window.location.href = "./shop.html"
    }
})
const validateLogin = () => {
    if(localStorage.getItem('token') !== null){
        window.location.href = "./shop.html"
    }
}
window.addEventListener('load',() => {
    validateLogin()
})
