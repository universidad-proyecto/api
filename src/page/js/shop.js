const contentProducts = document.getElementById("contentProducts")
const countCart = document.getElementById("countCart")


const addToCart = (id) => {
    console.log("Product add ",id);

    var cart = localStorage.getItem("cart")
    var swNew = true
    var count = 0

    if(cart == undefined){
        cart = "[]"
    }

    cart = JSON.parse(cart)

    cart = cart.map((e)=>{
        if(e.id == id){
            e.count++
            swNew = false
        }
        count+=e.count
        return e
    })

    if(swNew){
        cart.push({
            id,
            count:1
        })
        count++
    }

    localStorage.setItem("cart",JSON.stringify(cart))
    localStorage.setItem("count",count)

    countCart.innerHTML = count
}
const loadCart = () => {
    countCart.innerHTML = localStorage.getItem("count") || 0
}

const loadProduct = (product) => {
    const htmlProduct = `
    <div class="col mb-5">
        <div class="cart h-100">
            <!-- Product image-->
            <img class="cart-img-top" src="./img/${product.img || "default.jpg"}" alt="..." />
            <!-- Product details-->
            <div class="cart-body p-4">
                <div class="text-center">
                    <!-- Product name-->
                    <h5 class="fw-bolder">${product.name}</h5>
                    <p class="descriptionProducts">${product.description}</p>
                    <!-- Product price-->
                    <span class="text-muted text-decoration-line-through">${product.price}</span>
                    ${product.price * (1 - parseFloat(product.discount)/100)}
                </div>
            </div>
            <!-- Product actions-->
            <div class="cart-footer p-4 pt-0 border-top-0 bg-transparent">
                <div class="text-center">
                    <a class="btn btn-outline-dark mt-auto" onclick="addToCart(${product.id})" href="javascript:void(0)">Add to cart</a>
                </div>
            </div>
        </div>
    </div>
    `
    contentProducts.innerHTML+=htmlProduct
}

const loadProducts = async () => {
    const result = await getProducts()
    if(result.type == "ok" ){
        const products = result.result
        console.log("products",products)
        products.forEach(product => {
            loadProduct(product)
        });
    }
}

window.addEventListener("load",()=>{
    loadProducts()
    loadCart()
})

