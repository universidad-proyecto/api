
DROP TABLE IF EXISTS products_order;
DROP TABLE IF EXISTS inventore;
DROP TABLE IF EXISTS products;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS users;
CREATE TABLE users(  
    id SERIAL NOT NULL primary key,
    create_time NUMERIC,
    update_time NUMERIC,
    name varchar(255),
    email varchar(255) UNIQUE,
    password VARCHAR(255)
);
CREATE TABLE orders(  
    id SERIAL NOT NULL primary key,
    create_time NUMERIC,
    update_time NUMERIC,
    users_id INT,
    CONSTRAINT fk_users
      FOREIGN KEY(users_id) 
	  REFERENCES users(id),
    status varchar(255)  
);
CREATE TABLE products(  
    id SERIAL NOT NULL primary key,
    create_time NUMERIC,
    update_time NUMERIC,
    name varchar(255),
    description VARCHAR(255),
    img VARCHAR(255),
    discount NUMERIC,
    weigth NUMERIC,
    width NUMERIC,
    height NUMERIC,
    length NUMERIC ,
    price NUMERIC
);
CREATE TABLE inventore(  
    id SERIAL NOT NULL primary key,
    create_time NUMERIC,
    update_time NUMERIC,
    amount NUMERIC,
    product_id INT,
    CONSTRAINT fk_product
      FOREIGN KEY(product_id) 
	    REFERENCES products(id)
   
);
CREATE TABLE products_order(  
    id SERIAL NOT NULL primary key,
    create_time NUMERIC,
    update_time NUMERIC,
    amount NUMERIC,
    product_id INT,
    CONSTRAINT fk_product   
      FOREIGN KEY(product_id) 
	  REFERENCES products(id),
    order_id INT,
    CONSTRAINT fk_order  
      FOREIGN KEY(order_id) 
	  REFERENCES orders(id)
);



