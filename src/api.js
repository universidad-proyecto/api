require('module-alias/register')
const express = require('express');
const morgan = require('morgan');
const bodyParser = require ('body-parser')
const path = require('path');
const cors = require('cors');
const routes = require("@routes/index")


var app = express();
app.use(cors({origin:"*"}))


app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/api/v1",routes)

// captura 404 y reenvía al controlador de errores
app.use(function(req, res, next) {
  res.status(404).send({
    type:"error",
    msj: "No existe tal rutas"
  })
});

module.exports = app;